// Make Connection

var socket = io.connect('http://localhost:4000');


// Query DOM
var message = document.getElementById('message'),
    handle = document.getElementById('handle'),
    btn = document.getElementById('send'),
    output = document.getElementById('output'),
    feedback = document.getElementById('feedback');


var isSomeoneTyping = 0;
// Emit events

btn.addEventListener('click', function () {

    socket.emit('chat', {
        message: message.value,
        handle: handle.value,
    })
})


message.addEventListener('keypress', function () {
    socket.emit('typing', handle.value)
});

// Listen for events
socket.on('chat', function (data) {
    feedback.innerHTML = `<p><em></em></p>`
    output.innerHTML += '<p><strong>' + data.handle + ':</strong>' + data.message + '</p>';
})

// listening typing
socket.on('typing', function (data) {
    isSomeoneTyping = 1;
    feedback.innerHTML = `<p><em>${data} is typing a message...</em></p>`
});

// 
setInterval(() => {
    if (isSomeoneTyping <= 0) {
        feedback.innerHTML = '';
    } else {
        isSomeoneTyping -= 1;
    }
}, 1000);